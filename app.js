document.addEventListener("DOMContentLoaded", function () {
  const form = document.getElementById("form-buku");
  const listBelumSelesaiDibaca = document.getElementById(
    "list-belum-selesai-dibaca"
  );
  const listSelesaiDibaca = document.getElementById("list-selesai-dibaca");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    const judulInput = document.getElementById("judul");
    const penulisInput = document.getElementById("penulis");
    const tahunInput = document.getElementById("tahun");
    const selesaiCheckbox = document.getElementById("selesai");

    const judul = judulInput.value;
    const penulis = penulisInput.value;
    const tahun = parseInt(tahunInput.value);
    const selesai = selesaiCheckbox.checked;

    const book = {
      id: +new Date(),
      title: judul,
      author: penulis,
      year: tahun,
      isComplete: selesai,
    };

    if (selesai) {
      addBookToShelf(book, listSelesaiDibaca);
    } else {
      addBookToShelf(book, listBelumSelesaiDibaca);
    }

    updateLocalStorage();
    form.reset();
  });

  function addBookToShelf(book, shelf) {
    const bookItem = createBookItem(book);
    shelf.appendChild(bookItem);
  }
  function createBookItem(book) {
    const bookItem = document.createElement("li");
    bookItem.dataset.id = book.id;
    bookItem.dataset.author = book.author; // Tambahkan dataset untuk author
    bookItem.dataset.year = book.year; // Tambahkan dataset untuk tahun

    const bookInfo = document.createElement("div");
    bookInfo.innerHTML = `
      <strong>${book.title}</strong> (Penulis: ${book.author}, Tahun: ${book.year})
    `;
    const actionsContainer = document.createElement("div");
    actionsContainer.innerHTML = `
      <button class="pindahkan-btn">Pindahkan</button>
      <button class="edit-btn">Edit</button>
      <button class="hapus-btn">Hapus</button>
    `;

    bookItem.appendChild(bookInfo);
    bookItem.appendChild(actionsContainer);

    const pindahkanBtn = actionsContainer.querySelector(".pindahkan-btn");
    const editBtn = actionsContainer.querySelector(".edit-btn");
    const hapusBtn = actionsContainer.querySelector(".hapus-btn");

    pindahkanBtn.addEventListener("click", function () {
      moveBook(bookItem);
    });

    editBtn.addEventListener("click", function () {
      editBook(bookItem);
    });

    hapusBtn.addEventListener("click", function () {
      deleteBook(bookItem);
    });

    return bookItem;
  }

  function moveBook(bookItem) {
    const shelfId = bookItem.parentElement.id;
    const targetShelf =
      shelfId === "list-belum-selesai-dibaca"
        ? listSelesaiDibaca
        : listBelumSelesaiDibaca;

    // Perbaiki bagian ini untuk mendapatkan dataset yang benar
    const author = bookItem.dataset.author || "";
    const year = bookItem.dataset.year || "";

    const book = {
      id: bookItem.dataset.id,
      title: bookItem.querySelector("strong").textContent,
      author: author,
      year: year,
      isComplete: shelfId === "list-selesai-dibaca",
    };

    targetShelf.appendChild(createBookItem(book));
    deleteBook(bookItem);
    updateLocalStorage();
  }

  function editBook(bookItem) {
    const strongElement = bookItem.querySelector("strong");
    const currentTitle = strongElement ? strongElement.textContent : "";

    const newTitle = prompt("Edit Title:", currentTitle);
    if (newTitle !== null) {
      strongElement.textContent = newTitle;
      updateLocalStorage();
    }
  }

  function deleteBook(bookItem) {
    bookItem.remove();
    updateLocalStorage();
  }

  function updateLocalStorage() {
    const booksBelumSelesaiDibaca = Array.from(
      listBelumSelesaiDibaca.children
    ).map((bookItem) => getBookFromItem(bookItem));
    const booksSelesaiDibaca = Array.from(listSelesaiDibaca.children).map(
      (bookItem) => getBookFromItem(bookItem)
    );

    localStorage.setItem(
      "booksBelumSelesaiDibaca",
      JSON.stringify(booksBelumSelesaiDibaca)
    );
    localStorage.setItem(
      "booksSelesaiDibaca",
      JSON.stringify(booksSelesaiDibaca)
    );
  }

  function getBookFromItem(bookItem) {
    const strongElement = bookItem.querySelector("strong");

    let title = "";
    let author = "";
    let year = "";

    if (strongElement) {
      title = strongElement.textContent || "";
    }

    const authorElement = bookItem.dataset.author;
    const yearElement = bookItem.dataset.year;

    // Use parseInt to convert the year string to a number
    const book = {
      id: bookItem.dataset.id,
      title: title,
      author: authorElement,
      year: parseInt(yearElement),
      isComplete: bookItem.parentElement.id === "list-selesai-dibaca",
    };

    return book;
  }

  function loadBooksFromLocalStorage() {
    const booksBelumSelesaiDibaca =
      JSON.parse(localStorage.getItem("booksBelumSelesaiDibaca")) || [];
    const booksSelesaiDibaca =
      JSON.parse(localStorage.getItem("booksSelesaiDibaca")) || [];

    booksBelumSelesaiDibaca.forEach((book) =>
      addBookToShelf(book, listBelumSelesaiDibaca)
    );
    booksSelesaiDibaca.forEach((book) =>
      addBookToShelf(book, listSelesaiDibaca)
    );
  }

  loadBooksFromLocalStorage();
});
